#include <stdlib.h>
#include "constprop.h"
#include "constpropdef.h"
#include "sym.h"
#include "list.h"
#include "error.h"

#include "util.h"
#include "globals.h"

/*
    NOP,		 No operation. 
	ADD,		 Add. 
	BT,		 Branch to op[2] if op[0] is true. 
	BF,		 Branch to op[2] if op[0] is false. 
	BA,		 Branch always. 
	DIV,		 Divide. 
	GET,		 Get integer from stdin. 
	LABEL,		 A label. 
	LD,		 Load from memory. 
	MOV,		 Copy. 
	MUL,		 Multiply. 
	NEG,		 Negate. 
	PHI,		 Phi-function. 
	PUT,		 Print integer to stdout. 
	RET,		 Return. 
	SEQ,		 Set to true if ==. 
	SGE,		 Set to true if >=. 
	SGT,		 Set to true if >. 
	SLE,		 Set to true if <=. 
	SLL,		 Shift left logical. 
	SLT,		 Set to true if <. 
	SRA,		 Shift right arithmetic. 
	SRL,		 Shift right logical. 
	SNE,		 Set to true if !=. 
	ST,		 Store to memory. 
	SUB,		 Subtract. 
	NSTMT
*/

cvalue_t cTOP; // = newCValue(TOP, 0);
cvalue_t cBOT; // = newCValue(BOT, 0);

typedef struct {
	vertex_t *v, *u;
	int idx;
} arc_t;

typedef struct {
	vertex_t *w;
	stmt_t *s;
} ssa_workitem_t;

list_t* arcw; //Arc worklist
list_t* ssaw; //SSA worklist

arc_t* makeArc(vertex_t *v,vertex_t *u){
	arc_t* arc = malloc(sizeof(arc_t));
	*arc = (arc_t){.v = v, .u = u, .idx = (v->succ[1] == u?1:0)};
	return arc;
}

bool executable(arc_t* arc) {
	return arc->v->exec[arc->idx];
}

void set_executable(arc_t* arc) {
	arc->v->exec[arc->idx] = true;
}

ssa_workitem_t* makewi(vertex_t *w, stmt_t *s) {
	ssa_workitem_t* wi = malloc(sizeof(ssa_workitem_t));
	*wi = (ssa_workitem_t){.w = w, .s = s};
	return wi;
}

vertex_t* findVertexfromStmt(stmt_t* t) {
	return (vertex_t*) t->vertex;
}

void copyUsesToWorklist(sym_t* def) {
	if(def->uses == NULL) {
		return;
	}
	stmt_t *use;
	list_t *p, *h;
	p = h = def->uses;
	do {
		use = p->data;
		p = p->succ;

		vertex_t* vert = findVertexfromStmt(use);
		insert_last(&ssaw, makewi(vert, use));

	} while(p != h);
}

bool cond_branch(bool expects, cvalue_t cval) {
	switch(cval.ctype) {
		case TOP:
		case BOT:
			return true;
		case CONST:
			return	(cval.cvalue && expects) ||
					(!cval.cvalue && !expects);
	}
	return true;
}
/*
//enum for constant propagation
typedef enum {
 TOP = 0, CONST = 1, BOT = 2
} ctype_t;
// also for const prop.
typedef struct {
	ctype_t		ctype;		 //for constant propagation 
	int			cvalue;		 //Value stored here if ctype == VAL 
} cvalue_t;
*/

cvalue_t newCValue(ctype_t ctype, int cval){ 
	return (cvalue_t){.ctype = ctype, .cvalue = cval};
}

cvalue_t opToCValue(op_t op) {
	cvalue_t cval = cTOP;
	if(op.type == NUM_OP) {
		cval = newCValue(CONST, op.u.num);
	} else if (op.type == SYM_OP) {
		cval = op.u.sym->cval;
	}
	return cval;
}

typedef int(*constValFunc)(int, int);

cvalue_t constValOP(cvalue_t a, cvalue_t b, constValFunc func) {
	if (a.ctype == BOT || b.ctype == BOT)
		return cBOT;
	if (a.ctype == TOP || b.ctype == TOP)
		return cTOP;
	return newCValue(CONST, func(a.cvalue, b.cvalue));
}

void addUse(op_t op, stmt_t* t){
	if (op.type == SYM_OP) {
		list_t** uses = &(op.u.sym->uses);
		remove_from_list(uses, t);
		insert_last(uses, t);
	}
}

void doConstValOp(stmt_t* t, constValFunc func){
	sym_t* def = defined_sym(t);
	addUse(t->op[0], t);
	addUse(t->op[1], t);
	cvalue_t a = opToCValue(t->op[0]);
	cvalue_t b = opToCValue(t->op[1]);
	cvalue_t c = def->cval;
	cvalue_t ret = constValOP(a,b,func);
	if(ret.ctype < c.ctype){
		def->cval = ret;
		copyUsesToWorklist(def);
	}
}

int addi(int a, int b) { return a + b; }
int muli(int a, int b) { return a * b; }
int subi(int a, int b) { return a - b; }
int divi(int a, int b) { return a / b; }
int seqi(int a, int b) { return a == b; }
int snei(int a, int b) { return a != b; }
int sgei(int a, int b) { return a >= b; }
int sgti(int a, int b) { return a > b; }
int slei(int a, int b) { return a <= b; }
int slti(int a, int b) { return a < b; }
int srli(int a, int b);
int slli(int a, int b) { 
	if(b < 0) {
		return srli(a, -b);
	}
	return a << b;
}

/* This one was quite difficult because the C standard
 * doesn't specify if >> is logical or arithmetic though
 * it is usually logical right shift.
*/
int srai(int a, int b) {
	if(b <= 0) {
		return a << (-b);
	}
	if(a > 0 || ((-1) >> 1) == -1) {
		//Then >> is arithmetic right shift or a is positive
		return a >> b;
	}
	const int m1 = (1 << (32 - b))-1;      //0001 1111
	const int m2 = ~m1;                    //1110 0000
	return (a >> b) | m2;
}

/* This one was quite difficult because the C standard
 * doesn't specify if >> is logical or arithmetic though
 * it is usually logical right shift.
*/
int srli(int a, int b) {
	if(b <= 0) {
		return a << (-b);
	}
	if(a > 0 || ((-1) >> 1) != -1) {
		// Then >> is logical right shift
		// or a is positive integer so it doesn't matter
		return a >> b;
	}
	const int m1 = (1 << (32 - b))-1;      //0001 1111
	return (a >> b) & m1;
}

cvalue_t constMeet(cvalue_t a, cvalue_t b) {
	if (a.ctype == BOT || b.ctype == BOT)
		return cBOT;
	if (a.ctype == TOP && b.ctype == TOP)
		return cTOP;
	if (a.ctype == TOP && b.ctype == CONST)
		return b;
	if (a.ctype == CONST && b.ctype == TOP)
		return a;
	if(a.cvalue == b.cvalue)
		return a;
	return cBOT;
}

bool visit_stmt(vertex_t* w, stmt_t* t) {
	pr("Visiting stmt %d in vertex %d\n", t->index, w->dfnum);
	cvalue_t cval;
	bool expects = false;
	bool ret = false;
	switch(t->type) {
		//note: The BA case should not add arc the BF or BT
		//in that vertex is determined to be unconditional.
		case BA:	pr("inserting arc %d --> %d\n", w->dfnum, w->succ[1]->dfnum);
					insert_last(&arcw, makeArc(w, w->succ[1]));
					ret = true;
					break;
		case BT:	expects = true;
		case BF:	addUse(t->op[0], t);
					cval = opToCValue(t->op[0]);
					if(cond_branch(expects, cval)) {
						pr("inserting arc %d --> %d\n", w->dfnum, w->succ[0]->dfnum);
						insert_last(&arcw, makeArc(w, w->succ[0]));
						if(cval.ctype == CONST) {
							ret = true;
						}
					}
					break;
		case ADD:	doConstValOp(t, addi); break;
		case SUB:	doConstValOp(t, subi); break;
		case MUL:	doConstValOp(t, muli); break;
		case DIV:	doConstValOp(t, divi); break;
		case SEQ:	doConstValOp(t, seqi); break;
		case SNE:	doConstValOp(t, snei); break;
		case SGE:	doConstValOp(t, sgei); break;
		case SGT:	doConstValOp(t, sgti); break;
		case SLE:	doConstValOp(t, slei); break;
		case SLT:	doConstValOp(t, slti); break;
		case SLL:	doConstValOp(t, slli); break;
		case SRA:	doConstValOp(t, srai); break;
		case SRL:	doConstValOp(t, srli); break;
		
		case PHI:
			cval = cTOP;
			list_t *p, *h;
			vertex_t *pred;
			int n = -1;
			p = h = w->pred;

			if(p == NULL) {
				break;
			}
			
			do {
				pred = p->data;
				p = p->succ;
				n++;

				arc_t arc = {
					.v = pred,
					.u = w,
					.idx = (pred->succ[1] == w)
				};

				if (executable(&arc)) {
					if (n > t->phi->n) {
						error(FATAL, "Index out of bounds on phi-func operands.");
					}
					// value of phi-function operand for pred
					op_t op = t->phi->rhs[n];
					addUse(op, t);
					cvalue_t value = opToCValue(op);
					cval = constMeet(cval, value);
				}
			} while(p != h);
			sym_t* def = defined_sym(t);
			if (cval.ctype < def->cval.ctype) {
				def->cval = cval;
				copyUsesToWorklist(def);
			}
			
			break;
		case GET:	//Get and load are non-const.
		case LD:	//Always have unknown value!!
			def = defined_sym(t);
			def->cval = cBOT;
			break;
		case MOV:
			def = defined_sym(t);
			if(def != NULL) {
				addUse(t->op[0], t);
				def->cval = opToCValue(t->op[0]);
			}
			break;
		case NEG:
			def = defined_sym(t);
			if(def != NULL) {
				addUse(t->op[0], t);
				cvalue_t cval = opToCValue(t->op[0]);
				if(cval.ctype == CONST) {
					cval.cvalue = -cval.cvalue;
				}
				def->cval = cval;
			}
			break;
		case ST:
		case RET:
		case PUT:
		case LABEL:
		case NSTMT:
		case NOP: break;
		
	}
	sym_t* def = defined_sym(t);
	if(def != NULL) {
		cvalue_t cval = def->cval;
		pr("%s@{%d, %d} \t<--\t ", def->id,  cval.ctype, cval.cvalue);
	} else {
		pr("\t\t<--\t ");
	}

	if(t->op[0].type != NO_OP) {
		cvalue_t a = opToCValue(t->op[0]);
		if(t->op[0].type == SYM_OP) {
			pr("%s@", t->op[0].u.sym->id);
		}
		pr("{%d, %d}\t", a.ctype, a.cvalue);
	}
	if(t->op[1].type != NO_OP) {
		cvalue_t a = opToCValue(t->op[1]);
		if(t->op[1].type == SYM_OP) {
			pr("%s@", t->op[1].u.sym->id);
		}
		pr("{%d, %d}", a.ctype, a.cvalue);
	}
	pr("\n");

	return ret;
}

void visit_vertex(vertex_t* w) {
	pr("Visiting vertex %d. ", w->dfnum);
	bool onlyphi = w->visited;
	pr("\t onlyphi = %d\n", onlyphi);
	list_t *p, *h;
	stmt_t *t;
	bool early = false;

	w->visited = true;
	p = h = w->stmts;
	if(p != NULL) {
		do {
			t = p->data;
			p = p->succ;
			if (!onlyphi || t->type == PHI) {
				if (visit_stmt(w, t)) {
					//pr("Early exit\n");
					early = true;
					//break;
				}
			}
		} while(p!=h);
	}
	if (!early && !onlyphi) {
		if(w->succ[1] == NULL) {
			pr("Found Exit Node! =D");
			return;
		}
		//Found no branch stmts when examining all stmts
		//So we "fallthrough" to the next one
		pr("inserting arc %d --> %d\n", w->dfnum, w->succ[1]->dfnum);
		insert_last(&arcw, makeArc(w, w->succ[1]));
	}
}


void initialize(func_t* func) {
	cTOP = newCValue(TOP, 0);
	cBOT = newCValue(BOT, 0);

	arcw = NULL;
	ssaw = NULL;

	vertex_t** vertex = func->vertex;
	for(int i=0;i < func->nvertex; i++) {
		stmt_t *t;
		list_t *p, *h;
		vertex_t* w = vertex[i];
		
		w->visited = false;
		w->exec[0] = false;
		w->exec[1] = false;
		
		p = h = w->stmts;
		if(p != NULL) {
			do {
				t = p->data;
				p = p->succ;
				
				t->vertex = w;

				sym_t* sym = defined_sym(t);
				if(sym != NULL) {
					set_sym_def(sym, t);
					sym->cval = cTOP;
				}

			} while(p!=h);
		}
	}
}

void updateOp(op_t* op) {
	//If it's a non-label symbol with const value
	//then replace with a num op.
	if(is_sym(*op) && !is_label(*op)) {
		cvalue_t cval = op->u.sym->cval;
		if (cval.ctype == CONST) {
			op->type = NUM_OP;
			op->u.num = cval.cvalue;
		}
	}
}

void updatePhi(stmt_t* t) {
	//note: don't replace phi-operads.
	//It's disrupting dce...
	// int n = t->phi->n;
	// op_t* rhs = t->phi->rhs;
	// for(int i = 0; i < n; i++ ) {
	// 	updateOp(&rhs[i]);
	// }
}

//TODO we could update the uses lists here. If needed...
void updateStmt(stmt_t* t) {
	//If it's defining a symbol with constant value
	//then replace with a move stmt.
	sym_t* def = defined_sym(t);
	if (def != NULL) {
		cvalue_t cval = def->cval;
		if(cval.ctype == CONST) {
			//If it's a phi func, remove from list of phi funcs.
			if (t->type == PHI) {
				remove_from_list(&t->vertex->phi_func_list, t);
				//Free the phi function rhs.
				if(t->phi != NULL) {
					free_phi(t->phi);
					t->phi = NULL;
				}
			}
			
			//Turn into a move instr
			t->type = MOV;
			t->op[0].type = NUM_OP;
			t->op[0].u.num = cval.cvalue;
			t->op[1].type = NO_OP;
			
			return;
		}
	}
	//If the defined symbol didn't have const value
	//Then maybe some of the operands still do?
	
	//If it's a phi function then use updatePhi
	if(t->type == PHI) {
		updatePhi(t);
		return;
	}
	
	//if it any of the input operands are const
	//then replace with constant value.
	updateOp(&t->op[0]);
	updateOp(&t->op[1]);
	
}

void updateOps(func_t* func) {

	//TODO update all statments now that values for symbols have been calculated.
	//Turn op->sym with constant value into op->num

	vertex_t** vertex = func->vertex;
	for(int i=0;i < func->nvertex; i++) {
		stmt_t *t;
		list_t *p, *h;
		vertex_t* w = vertex[i];
		
		p = h = w->stmts;
		if(p != NULL) {
			do {
				t = p->data;
				p = p->succ;
				
				updateStmt(t);
			} while(p!=h);
		}
	}

}


void cprop(func_t* func) {
	bool use_pr_old = use_pr;
	use_pr = false;
	
	initialize(func);

	visit_vertex(func->start);
	//visit_vertex(func->vertex[2]);
	

	while(arcw != NULL || ssaw != NULL) {
		if (arcw != NULL) {
			arc_t* arc = remove_first(&arcw);
			
			if (!executable(arc)) {
				set_executable(arc);
				visit_vertex(arc->u);
			}
			
			free(arc);
		}
		if(ssaw != NULL) {
			ssa_workitem_t* sp = remove_first(&ssaw);
			visit_stmt(sp->w, sp->s);
			free(sp);
		}
	}
	
	free_list(&arcw);
	free_list(&ssaw);

	updateOps(func);

	use_pr = use_pr_old;
}










































