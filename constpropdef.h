#ifndef _CONSTPROPDEF_H_
#define _CONSTPROPDEF_H_

//enum for constant propagation
typedef enum {
 TOP = 2, CONST = 1, BOT = 0
} ctype_t;

//struct for constant propagation
typedef struct {
	ctype_t		ctype;		/* for constant propagation */
	int			cvalue;		/* Value stored here if ctype == VAL */
} cvalue_t;

#endif