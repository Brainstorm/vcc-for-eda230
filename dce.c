#include <stdlib.h>
#include <assert.h>

#include "globals.h"
#include "util.h"
#include "sym.h"
#include "dce.h"
#include "error.h"

//We will need to calculate control dependency
//which is the dominance frontier of the reversed control flow graph.
static void calculate_ctrl_dep(func_t* func) {
	rdom(func);
	//pr("%s:%d\t%s\n", __FILE__, __LINE__, "Did it work?");
	//exit(1);
}

static bool isPrelive(stmt_t* t) {
	switch (t->type) {
	case GET:
	case PUT:
	case RET:
	case ST:
		return true;
	default:
		return false;
	}
}

static void live_op(op_t op, list_t** worklist) {
	if(is_sym(op) && !is_label(op)) {
		sym_t* s = op.u.sym;
		stmt_t* t = s->def;
		if (!t->live) {
			t->live = true;
			insert_first(worklist, t);
		}
	}
}

static void live_phi(phi_t* phi, list_t** worklist) {
	for(int i = 0; i < phi->n; i++) {
		live_op(phi->rhs[i], worklist);
	}
}

//Mark statments that change program output as live
//Mark vertecies as live when they contain at least one live statment
//Remove statments that are not live
static void find_live_code(func_t* func) {
	vertex_t* w;
	list_t *p,*h;
	stmt_t *t;
	list_t* worklist = NULL;
	
	//Initialization: find definitions, and mark prelives
	for(int i = 0; i < func->nvertex; i++) {
		w = func->vertex[i];
		p = h = w->stmts;
		if(p != NULL) do {
			t = p->data;
			p = p->succ;
			
			//probably already done but couldn't hurt, just in case.
			t->vertex = w;
			
			sym_t *s = defined_sym(t);
			if(s != NULL) {
				s->def = t;
			}
			
			t->live = isPrelive(t);
			if (t->live) {
				insert_first(&worklist, t);
			}
		} while (p != h);
	}
	
	//While the worklist is not empty
	while(worklist != NULL) {
		t = worklist->data;	
		remove_first(&worklist);
		
		t->vertex->live = true;	
		if(t->type == PHI) {
			live_phi(t->phi, &worklist);
		} else {
			live_op(t->op[0], &worklist);
			live_op(t->op[1], &worklist);
		}
		
		size_t n;
		vertex_t** a = (vertex_t**) set_to_array(t->vertex->cdinv, &n);
		for(int i = 0; i < n; i++) {
			vertex_t *v;
			stmt_t *ba, *bc;
			list_t* p;
			
			v = a[i];
			p = v->stmts;
	
			if(p == NULL) {
				continue;
			}
			
			ba = p->pred->data;
			bc = p->pred->pred->data;
			
			assert(ba->type == BA);
			if(!ba->live) {
				ba->live = true;
				insert_first(&worklist, ba);
			}
			
			// note: there isn't always a conditional branch...
			if(!bc->live && (bc->type == BT || bc->type == BF)) {
				bc->live = true;
				insert_first(&worklist, bc);
			}
		}
		free(a);
	}
	
	for(int i = 0; i < func->nvertex; i++) {
		w = func->vertex[i];
		p = h = w->stmts;
		if(p != NULL) do {
			t = p->data;
			p = p->succ;
			
			if(!t->live && t->type != LABEL && !is_branch(t)) {
				remove_from_list(&t->vertex->stmts, t);
				remove_from_list(&t->vertex->phi_func_list, t);
				free_stmt(t);
				t = NULL;
			}
		} while (p != h);
	}
}

static sym_t* find_label(vertex_t* v) {
	if(v->stmts) {
		stmt_t* s = v->stmts->data;
		if(s->type == LABEL && is_label(s->op[0])) {
			return s->op[0].u.sym;
		}
	}
	return NULL;
}

static void update_branch(vertex_t *u, vertex_t *v, vertex_t *w, int j) {
	// replace arc (u,v) with (u,w), see vertex_t::succ and vertex_t::pred
	assert(u->succ[j] == v);
	u->succ[j] = w;
	
	// append u to w->pred
	insert_last(&w->pred, u);
	
	// update the branch in u to its new target w
	list_t *p;
	stmt_t *bc, *ba;
	
	sym_t* vlabel = find_label(v);
	sym_t* wlabel = find_label(w);
	
	p = u->stmts;
	
	if(p != NULL) {
		ba = p->pred->data;
		bc = p->pred->pred->data;
		
		// note: expected Branch Always
		assert(ba->type == BA);
		if(is_label(ba->op[2]) && ba->op[2].u.sym == vlabel) {
			ba->op[2].u.sym = wlabel;
		}
		
		// note: there isn't always a conditional branch...
		if((bc->type == BT || bc->type == BF) && is_label(bc->op[2]) &&
			bc->op[2].u.sym == vlabel) {
			bc->op[2].u.sym = wlabel;
		}
	}
	
}

//Does v dominate u?
static bool dominates(vertex_t* v, vertex_t* u) {
	vertex_t* c = u;
	while(c != NULL && c != v) {
		c = c->idom;
	}
	return c == v;
}

//TODO needs rework. can it be merged with update_branch?
static void update_phi_operands(vertex_t* u, vertex_t* w) {
	//for each phi func
	list_t *p, *h;
	stmt_t *t;
	
	// u has been appended to w->pred
	// so now we need to append the correct
	// symbol to all phi-funcs in w
	
	p = h = w->stmts;
	if(p != NULL) {
		do {
			t = p->data;
			p = p->succ;
			
			// Using w->phi_func_list didn't work...
			if(t->type != PHI) {
				continue;
			}
			
			int n = t->phi->n;
			op_t* rhs = t->phi->rhs;
			
			// Select symbol
			sym_t* d = NULL;
			int i = 0;
			do {
				if(rhs[i].type == SYM_OP) {
					d = rhs[i].u.sym;
				}
				i++;
			} while(i < n && d == NULL);
			
			if(d == NULL) {
				// If all are NUM_OPs?
				// I've got no idea!
				// I'll find out later
				assert("Not implemented" == NULL);
			}
			
			while(i < n && !dominates(d->def->vertex, u)) {
				if(rhs[i].type == SYM_OP) {
					d = rhs[i].u.sym;
				}
				i++;
			}
			
			if(!dominates(d->def->vertex, u)) {
				// If all sym_ops definition vertex does not dominate u
				// Can we even reach this?
				assert("Should not happen" == NULL);
			}
			
			for(;i < n;i++) {
				if(rhs[i].type != SYM_OP){
					continue;
				}
				sym_t* sy = rhs[i].u.sym;
				if(dominates(sy->def->vertex, u)) {
					if(dominates(d->def->vertex, sy->def->vertex)) {
						d = sy;
					}
				}
			}
			
			// Make space in rhs for more stuff.
			op_t* tmp = malloc(sizeof (op_t) * (n + 1));
			for(int i = 0; i < n; i++) {
				tmp[i] = rhs[i];
			}
			// Update locals
			n++;
			rhs = tmp;
			// Update vertex
			t->phi->rhs = rhs;
			t->phi->n = n;
			
			// Now put some data in there...
			rhs[n-1] = new_sym_op(d);
			
		} while(p != h);
	}
}

#if 0
	p = h = w->phi_func_list;
	if(p != NULL) {
		do {
			t = p->data;
			p = p->succ;
			
			int n = t->phi->n;
			op_t* rhs = t->phi->rhs;
			for(int i = 0; i < n; i++ ) {
				op_t op = rhs[i];
				if (is_sym(op) && !is_label(op)) {
					vertex_t* defv = op.u.sym->def->vertex;
					if (dominates(defv,  u)) {
						//TODO Something?
					}
				}

				//	vertex(rhs[i]) = u;
			}
		} while(p!=h);
	}
#endif

//Remove vertecies that are not live
static void simplify(func_t* func) {
	func->exit->live = true;
	bool modified = false;
	
	for(int i = 0; i < func->nvertex; i++) {
		vertex_t* u = func->vertex[i];
		if(!u->live) {
			continue;
		}
		
		for(int j = 0; j < 2; j++) {
			vertex_t* v = u->succ[j];
			if(v == NULL || v->live) {
				continue;
			}
			vertex_t* w = v->ipdom;
			
			while(!w->live) {
				w = w->ipdom;
			}
			
			update_branch(u, v, w, j);
			update_phi_operands(u, w);
			modified = true;
		}
	}
	if (modified) {
		for(int i = 0; i < func->nvertex; i++) {
			vertex_t* v = func->vertex[i];
			//TODO "remove" vertex v
			//Do we really need this step?
			//The removed vertexes are not reachable anymore right?
			v = v;
		}
		
		// update dominator tree
		dominance(func);
	}
}

void dce(func_t* func) {
	bool old_use_pr = use_pr;
	use_pr = true;
	
	calculate_ctrl_dep(func);
	find_live_code(func);
	if(0) simplify(func);
	
	use_pr = old_use_pr;
}





