#ifndef _DCE_H_
#define _DCE_H_

#include "func.h"

//Applies dead code elimination
void dce(func_t* func);

//Calculates reverse dominance frontier
void rdom(func_t* func);


#endif // _DCE_H_