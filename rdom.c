#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "util.h"
#include "cfg.h"
#include "func.h"
#include "sym.h"
#include "globals.h"
#include "error.h"

#include "dce.h"

static bool visited(vertex_t* v) {
	return v == NULL || v->rdfnum != -1;
}

static void dfs(vertex_t* v, int* rdfnum, vertex_t** vertex) {
	list_t *p, *h;
	vertex_t* w;
	
	if(v == NULL) {
		return;
	}
	
	pr("Vertex %d: rdfnum = %d\n", v->index, *rdfnum);
	v->rdfnum = *rdfnum;
	vertex[*rdfnum] = v;
	v->spdom = v;
	v->pancestor = NULL;
	(*rdfnum)++;
	
	p = h = v->pred;
	
	if(p != NULL) do {
		w = p->data;
		p = p->succ;
		
		if(!visited(w)){
			w->pparent = v;
			dfs(w, rdfnum, vertex);
		}
		
	} while(p != h);
	
}

static void link(vertex_t* pparent, vertex_t* pchild) {
	pchild->pancestor = pparent;
	pr("ancestor(%d) = %d\n", pchild->index, pparent->index);
}

static vertex_t* eval(vertex_t* v) {
	vertex_t*		u;

	u = v;

	/* Find the ancestor of V with least semi-dominator. */
	while (v->pancestor != NULL) {

		if (v->spdom->rdfnum < u->spdom->rdfnum) {
			u = v;
		}

		v = v->pancestor;
	}
	
	return u;
}

static void rdominance(func_t* func) {
	int			i;
	int			j;
	int			rdfnum;
	vertex_t*	u;
	vertex_t*	v;
	vertex_t*	w;
	list_t*		p;
	list_t*		h;
	vertex_t*	rvertex[func->nvertex];
	
	memcpy(rvertex, func->vertex, sizeof rvertex);
	
	if (0) visited(NULL);	/* For silencing GCC. */
	
	/* Construct the immediate-dominator tree. */

	/* Step 1. */

	/* Initialise sdom of each vertex to itself. */
	for (i = 0; i < func->nvertex; i++)	{
		rvertex[i]->rdfnum			= -1;
		rvertex[i]->spdom			= rvertex[i];
		rvertex[i]->ipdom			= NULL;
		rvertex[i]->pancestor		= NULL;
		rvertex[i]->pparent			= NULL;
		rvertex[i]->dompchild		= NULL;
		rvertex[i]->dompsibling		= NULL;
		
		//Make sure that the bucket is empty...
		assert(rvertex[i]->bucket == NULL);
		
		if (rvertex[i] == func->exit) {
			u = rvertex[0];
			rvertex[0] = func->exit;
			rvertex[i] = u;
		}
	}

	rdfnum = 0;
	
	assert(rvertex[0] == func->exit);

	dfs(rvertex[0], &rdfnum, rvertex);
	
	assert(rvertex[0] == func->exit);
	
	pr("dfnum = %d\n", rdfnum);
	pr("n     = %d\n", func->nvertex);
	// func->nvertex = rdfnum;
	
	print_cfg(func);
	
	for (i = func->nvertex - 1; i > 0; i--) {
		/* Step 2. */
		w = rvertex[i];
		
		pr("\nstep 2 for %d:%d\n", w->index, w->rdfnum);

		// All vertices except the exit node must have a successor.
		assert(i == 0 || w->succ[0] != NULL || w->succ[1] != NULL);
		
		for(j = 0; j < MAX_SUCC; j++) {
			v = w->succ[j];
			if(v == NULL) {
				continue;
			}
			
			u = eval(v);

			pr("succ %d\n", v->index);
			pr("eval(%d) = %d\n", v->index, u->index);
			pr("spdom(%d) = %d\n", u->index, u->spdom->index);

			/* Complete? */
			if(u->spdom->rdfnum < w->spdom->rdfnum) {
				w->spdom = u->spdom;
			}
		};
		
/*		Blah!
		if(w->sdom == w->parent) {
			w->idom = w->parent;
		} else {
			insert_before(w->sdom->bucket)
		}
	*/	
		pr("spdom(%d) = %d\n", w->index, w->spdom->index);

		link(w->pparent, w);
	
		/* Step 3. */
		insert_first(&w->spdom->bucket, w);

		pr("emptying bucket of %d\n", w->pparent->index);

		p = h = w->pparent->bucket;

		if (p == NULL){
			continue;
		}
		
		do {
			v = p->data;
			p = p->succ;
			u = eval(v);
			
			/* COMPLETE! */
			
			if(u->spdom->rdfnum < v->spdom->rdfnum) {
				v->ipdom = u;
				pr("ipdom(%d) = %d\n", v->index, u->index);
			} else {
				v->ipdom = w->pparent;
				pr("ipdom(%d) = %d\n", v->index, w->pparent->index);
			}
		} while (p != h);

		free_list(&w->pparent->bucket);
	}

	/* Step 4. */
	pr("\nstep 4\n", "");
	for (i = 1; i < func->nvertex; i++) {
		
		w = rvertex[i];
		
		if(w->ipdom != w->spdom) {
			w->ipdom = w->ipdom->ipdom;
		}
		
		w->dompsibling = w->ipdom->dompchild;
		w->ipdom->dompchild = w;
		
		pr("final ipdom(%d) = %d\n", w->index, w->ipdom->index);
	}
	
	print_dt(func);
}

static void cdinv(vertex_t* u) {
	if(u == NULL){
		return;
	}
	
	cdinv(u->dompchild);
	cdinv(u->dompsibling);
	
	vertex_t* pchild;
	list_t *p, *h;
	
	p = h = u->pred;
	
	if(p) do {
		pchild = p->data;
		p      = p->succ;
		
		if(pchild->ipdom != u) {
			//add to cdinv
			join_set(&(u->cdinv),pchild);
		}
		
	} while(p != h);
	
	
	pchild = u->dompchild;
	while(pchild != NULL) {
		size_t n;
		vertex_t** a = (vertex_t**) set_to_array(pchild->cdinv, &n);
		for(int i = 0; i < n; i++){
			vertex_t* v = a[i];
			if(v->ipdom != u){
				join_set(&(u->cdinv), v);
			}
		}
		free(a);
		pchild = pchild->dompsibling;
	}
	
}

void print_vertex_index(void* data, FILE* fp) {
	vertex_t* v = data;
	fprintf(fp, "%d", v->index);
}

void rdom(func_t* func) {
	bool old_use_pr;
	
	old_use_pr = use_pr;
	use_pr = false;
	
	rdominance(func);
	cdinv(func->exit);
	
#ifdef PRDEBUG
	if(use_pr) {
		for (int i = 0; i < func->nvertex; i++) {
			vertex_t* v = func->vertex[i];
			printf("cdinv(%d) = ", v->index);
			print_set(v->cdinv, print_vertex_index, stdout);
		}
	}
#endif
	
	use_pr = old_use_pr;
}
